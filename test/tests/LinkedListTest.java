package tests;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import model.data_structures.Lista;
import model.data_structures.Lista.Node;

public class LinkedListTest<T extends Comparable <T>> {

	Lista <T> lista1;

	T elemento1;

	T elemento2;


	@Before
	public void setup1()
	{
		lista1 = new Lista();
	}


	@Test
	public void testAdd()
	{
		setup1();

		lista1.add(elemento1);

		assertEquals(1, lista1.size());
	}

	@Test
	public void testDelete()
	{
		setup1();

		lista1.add(elemento1);

		lista1.add(elemento2);

		lista1.delete(elemento2);

		assertEquals(elemento1, lista1.get(elemento1));
	}

	@Test
	public void testGet()
	{
		setup1();

		lista1.add(elemento2);

		assertNull(lista1.get(elemento1));
	}

	@Test 
	public void testSize()
	{
		setup1();

		lista1.add(elemento1);

		lista1.add(elemento2);

		assertEquals(2, lista1.size());
	}

	@Test
	public void testPos()
	{
		setup1();

		lista1.add(elemento1);

		lista1.add(elemento2);


		assertEquals(elemento1, lista1.getByPos(1));
	}

	@Test 
	public void testCurrent()
	{
		setup1();

		lista1.add(elemento1);

		lista1.add(elemento2);

		assertEquals(elemento2, lista1.getCurrent());
	}
	
	@Test 
	public void testNext()
	{
		setup1();

		lista1.add(elemento1);

		lista1.add(elemento2);
		
		lista1.listing();
		
		assertEquals(elemento2, lista1.next());
	}
}
