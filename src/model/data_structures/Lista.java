package model.data_structures;

public class Lista <T extends Comparable <T>> implements LinkedList<T>{


	Node primerNodo;

	Node nodoActual;
	
	int size;

	/** Clase interna que representa un nodo*/

	public class Node
	{
		T item;

		Node next;

		Node prev;
 
		public Node()
		{
			T item = null;
		}

		Node NodoActual = this;

		public Node next()
		{
			return next;
		}
		
		public T item()
		{
			return item;
		}

	}


	public Lista ()
	{
		primerNodo = null;
		nodoActual = null;
		size = 0;
	}

	@Override
	public void add(T ele) {
		// TODO Auto-generated method stub

		Node nuevo = new Node();

		if(primerNodo!= null)
		{
			if(primerNodo.next != null)
			{
				nodoActual.next = nuevo;

				nuevo.prev = nodoActual;

				nuevo.item = ele;

				nodoActual = nuevo;
				
				size++;

			}
			else
			{
				primerNodo.next = nuevo;

				nuevo.item = ele;

				nuevo.prev = primerNodo;

				nodoActual = nuevo;
				
				size++;
			}
			//		if (primerNodo != null)
			//		{
			//			Node actual = primerNodo;
			//
			//			Node nuevo = new Node();
			//
			//			nuevo.item = ele;
			//
			//			while (actual.next() != null)
			//			{
			//				actual = actual.next();
			//			}
			//
			//			actual.next= nuevo;
			//
			//			nuevo.item = ele;
			//
			//			nuevo.prev = actual;
			//
			//			ultimoNodo = nuevo;

		}

		else
		{
			primerNodo = new Node();
			primerNodo.item = ele;
			nodoActual = primerNodo;
			size++;

		}


	}

	@Override
	public void delete(T ele) {
		// TODO Auto-generated method stub

		if(primerNodo!= null)
		{
			if(primerNodo.item == ele)
			{
				if(primerNodo.next() != null)
				{
					Node remplazar = primerNodo.next();

					primerNodo = remplazar;
					size--;
				}

				else
				{
					primerNodo = null;		
					size--;
				}

			}

			else
			{
				Node actual = primerNodo;

				boolean encontro = false;

				while (actual.next() != null && encontro == false)
				{
					Node next = actual.next();

					if (next.item == ele)
					{
						encontro = true;

						if(next.next == null)
						{
							next = null;
							
							size--;

						}

						else
						{
							Node siguiente = next.next();

							actual.next = siguiente;

							siguiente.prev = actual;
							
							size--;
						}
					}

					actual = actual.next;
				}
			}
		}

	}

	@Override
	public T get(T ele) {
		// TODO Auto-generated method stub

		T rta = null;

		Node actual = primerNodo;

		if( primerNodo.item == ele)
		{
			rta = primerNodo.item;
		}

		else
		{
			boolean encontro = false;

			while (actual != null && encontro == false)
			{
				if(actual.item == ele)
				{
					rta = (T) actual.item;

					encontro = true;
				}
			}
		}

		return rta;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
//		int rta = 0;
//
//		Node actual = primerNodo;
//
//		while(actual != null)
//		{
//			rta++;
//			actual = actual.next();
//		}
//
//		return rta;
		
		return size;
	}

	@Override
	public T getByPos(int position) {
		// TODO Auto-generated method stub

//		T rta = null;
//
//		int cont = 0;
//
//		Node actual = primerNodo;
//
//		Boolean encontro = false;
//
//		while (actual != null && cont<position)
//		{
//			cont ++;
//			actual = actual.next();
//		}
//
//		if(cont == position && actual!= null)
//		{
//			rta =  (T) actual.item;
//		}
//
//		return rta;
		
		
			Node actual = primerNodo;
			while(position > 0) {
				actual = actual.next();
				position = position-1;
			}
			return (T) actual.item();
		}

	

	@Override
	public void listing() {
		// TODO Auto-generated method stub
		nodoActual = primerNodo;


	}

	@Override
	public T getCurrent() {
		// TODO Auto-generated method stub
		return (T) nodoActual.item;
	}

	@Override
	public T next() {
		// TODO Auto-generated method stub
		Node rta = nodoActual.next;
		return  (T) rta.item;
	}

	public Node primerNodo()
	{
		return primerNodo;
	}
	
	public Node actual()
	{
		return nodoActual;
	}

}
