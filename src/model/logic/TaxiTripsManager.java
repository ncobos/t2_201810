package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.Lista;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 

	Lista<Service> listaJson = new Lista<Service>();

	public void loadServices (String serviceFile) {
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with " + serviceFile);

		JsonParser parser = new JsonParser();

		try {
			String taxiTripsDatos = "./data/taxi-trips-wrvz-psew-subset-small.json";

			System.out.println("Objetos");

			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(taxiTripsDatos));



			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);

				if(obj == null )
				{
					System.out.println("El objeto es nulo en: " + i);
				}

				/* Mostrar un JsonObject (Servicio taxi) */
				//System.out.println("------------------------------------------------------------------------------------------------");



				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------				
				/* Obtener la propiedad dropoff_centroid_latitude de un servicio (String)*/
				String dropoff_centroid_latitude = "NaN";
				if ( obj.get("dropoff_centroid_latitude") != null )
				{ dropoff_centroid_latitude = obj.get("dropoff_centroid_latitude").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad dropoff_centroid_longitude de un servicio (String) */
				String dropoff_centroid_longitude = "NaN";
				if ( obj.get("dropoff_centroid_longitude") != null )
				{ dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad company de un servicio (String) */
				String company = "NaN";
				if ( obj.get("company") != null)
				{ company = obj.get("company").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad trip ID de un servicio (String) */
				String tripId = "NaN";
				if ( obj.get("trip_id") != null)
				{ tripId = obj.get("trip_id").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad trip seconds de un servicio (int) */
				int tripSec = 0;
				if ( obj.get("trip_seconds") != null)
				{ tripSec = obj.get("trip_seconds").getAsInt(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad trip miles de un servicio (double) */
				double tripMil= 0;
				if ( obj.get("trip_miles") != null)
				{ tripMil = obj.get("trip_miles").getAsDouble(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad trip total de un servicio (double) */
				double tripTot= 0;
				if ( obj.get("trip_total") != null)
				{ tripTot = obj.get("trip_total").getAsDouble(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad taxi ID de un servicio (String) */
				String taxiId = "NaN";
				if ( obj.get("taxi_id") != null)
				{ taxiId = obj.get("taxi_id").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad dropoff community area de un servicio (int) */
				int comArea = 0;
				if ( obj.get("dropoff_community_area") != null)
				{ comArea = obj.get("dropoff_community_area").getAsInt(); }


				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				JsonObject dropoff_localization_obj = null; 

				//System.out.println("(Lon= "+dropoff_centroid_longitude+ ", Lat= "+dropoff_centroid_latitude +") (Datos String)" );

				/* Obtener la propiedad dropoff_centroid_location (JsonObject) de un servicio*/
				if ( obj.get("dropoff_centroid_location") != null )
				{ dropoff_localization_obj =obj.get("dropoff_centroid_location").getAsJsonObject();

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad coordinates (JsonArray) de la propiedad dropoff_centroid_location (JsonObject)*/
				JsonArray dropoff_localization_arr = dropoff_localization_obj.get("coordinates").getAsJsonArray();

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener cada coordenada del JsonArray como Double */
				double longitude = dropoff_localization_arr.get(0).getAsDouble();
				double latitude = dropoff_localization_arr.get(1).getAsDouble();}
				//System.out.println( "[Lon: " + longitude +", Lat:" + latitude + " ] (Datos double)");

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


				Service servicio = new Service(tripId, tripSec, tripMil, tripTot, comArea, taxiId, company);

				listaJson.add(servicio);

			}

			System.out.println("En esta lista hay: " + listaJson.size());

		}




		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	}


	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub

		System.out.println("Inside getTaxisOfCompany with " + company);

		Lista <Taxi> listaTaxis = new Lista<Taxi>();

		Lista <String> listaCodigos = new Lista<String>(); 

		//		System.out.println(listaJson.size());

		for (int i = 0; i < listaJson.size() ; i++)
		{

			Service servicio = listaJson.getByPos(i);


			Taxi taxi = servicio.getTaxi();

			Boolean esta = false;

			String taxiId = taxi.getTaxiId();

			//			System.out.println(i);

			//			System.out.println("taxiId " +taxiId);

			for(int j = 0; j < listaCodigos.size(); j++)
			{
				String actual = listaCodigos.getByPos(j);

				if(actual!= null)
				{
					if(taxiId.equals(actual))
					{
						esta = true;
					}

					else
					{

					}
				}
			}

			String comparador = taxi.getCompany();

			//			System.out.println(comparador);

			//			System.out.println(i);
			if(comparador.contains(company) && esta == false)
			{

				//				if (i == 0)
				//				{
				//					listaCodigos.add(taxiId);
				//					listaTaxis.add(taxi);
				//					System.out.println("Holi");
				//				}
				//
				//				else
				//				{
				//
				//					for(int j = 0; j < listaTaxis.size(); j++)
				//					{
				//						Taxi taxiActual = listaTaxis.getByPos(j);
				//
				//						String codigoActual = null;
				//
				//						if(taxiActual!= null)
				//						{
				//							codigoActual = taxiActual.getTaxiId();
				//						}
				//
				//						System.out.println("codigoActual: " + codigoActual);
				//
				//						System.out.println(j);
				//						System.out.println("i" + i);
				//
				//						if(codigoActual != null)
				//						{
				//							if(codigoActual.equals(taxiId))
				//							{
				//								System.out.println("hola2");
				//								listaTaxis.add(taxi);
				//
				//							}
				//						}
				//					}
				//				}



				listaTaxis.add(taxi);

				listaCodigos.add(taxiId);
			}
		}

		for (int k = 0; k<listaCodigos.size(); k++)
		{
			String codigo = listaCodigos.getByPos(k);
			System.out.println("Codigo "+ k+ " "+ codigo);
		}
		return listaTaxis;


	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);

		Lista <Service> listaServicio = new Lista<Service>();

		for (int i = 0; i < listaJson.size() ; i++)
		{

			Service servicio = listaJson.getByPos(i);

			//			System.out.println(listaServicio.size());

			int comparador = servicio.getDropComArea();

			//			System.out.println(comparador);

			if(comparador == communityArea)
			{
				listaServicio.add(servicio);
			}
		}

		//		System.out.println(listaServicio.size());
		
		for(int k = 0; k < listaServicio.size(); k++)
		{
			Service actual = listaServicio.getByPos(k);
			
			String codigo = actual.getTripId();
			
			System.out.println("Trip Id: " + codigo);
		}

		return listaServicio;
	}

	public int numero()
	{
		int rta = 0;

		rta = listaJson.size();

		return rta;
	}


}
